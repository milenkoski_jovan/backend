#!/bin/bash

if [[ "$NODE_ENV" == "local" ]]; then
	echo $NODE_ENV
	node ./server/dst/hooray.js
else
	cd /usr/src/app
	npm install
	node ./server/dst/hooray.js
fi
