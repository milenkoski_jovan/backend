clean-server:
	rm -rf server/dst
	mkdir server/dst

compile-server: clean-server
	find server/src -name '*.ts' | xargs tsc --sourceMap --noImplicitAny --suppressImplicitAnyIndexErrors --nolib -m commonjs --outDir server/dst server/src/references.d.ts

test-server: 
	NODE_ENV=test ./node_modules/.bin/istanbul cover --root ./server ./server/all-tests.js