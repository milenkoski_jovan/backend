declare module 'blue-tape' {
    import Promise = require('bluebird');
    module t {
        export interface Tester {
            test(description:string, cb:(t:Tester) => Promise<any>):void;
            test(description:string, cb:(t:Tester) => void):void;
            ok(ok:any, description?:string):void;
            notOk(notOk:any, description?:string):void;
            equals<T>(p1:T, p2:T, description?:string):void;
            equal<T>(p1:T, p2:T, description?:string):void;
            deepEquals<T>(p1:T, p2:T, description?:string):void;
            deepEqual<T>(p1:T, p2:T, description?:string):void;
            end():void;
        }
    }
    var t: t.Tester;
    export = t;
}