declare module "duplexer2" {
    function duplexer(input:NodeJS.ReadWriteStream, output:NodeJS.ReadWriteStream):NodeJS.ReadWriteStream;

    export = duplexer;
}