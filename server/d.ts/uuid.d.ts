declare module 'uuid' {
    interface Options {
        random:number[]
        rng:Function
    }

    function uuid(options?:Options,buffer?:any,offset?:number):any

    module uuid {
        export function v1(options?:{node:any[]
                                     clockseq:number
                                     msecs:any
                                     nsecs:number},buffer?:any,offset?:number):any
        export function v4(options?:Options,buffer?:any,offset?:number):any
    }

    export = uuid;
}