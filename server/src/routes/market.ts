import _ = require('lodash');
import express = require('express');
import Promise = require('bluebird');
import router = require('simple-router');

import codedError = require('../lib/coded-error');

import bodyParser = require('./lib/body-parser');
import TM = require('./lib/transactionable-middleware');
import Path = require('path');

import uuid = require('uuid');
import db = require('../lib/base');


import TagManager = require('../products/product-manager');

var app = router();

app.get("/list/all", TM.wrapTx(req => {
    const ProductMng = TagManager.create(req.userTx);
    return ProductMng.getAllMarkets();
}));

app.get("/product/list/all", TM.wrapTx(req => {
    const ProductMng = TagManager.create(req.userTx);
    return ProductMng.getAllProducts();
}));

app.get("/:marketId/get", TM.wrapTx(req => {
    const ProductMng = TagManager.create(req.userTx);
    return ProductMng.getMarketById(req.params.marketId);
}));

app.get("/:marketId/products/get", TM.wrapTx(req => {   
    const ProductMng = TagManager.create(req.userTx);
    return ProductMng.getProductsByMarket(req.params.marketId);
}));

app.get("/product/:productName/get", TM.wrapTx(req => {
    const ProductMng = TagManager.create(req.userTx);
    return ProductMng.getProductsByName(req.params.productName);
}));

export var route = app.route;

