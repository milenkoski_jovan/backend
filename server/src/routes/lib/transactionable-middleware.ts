import express = require('express');
import Promise = require('bluebird');

import db = require('../../lib/base');

export interface AuthRequest extends express.Request {
    session:any
    sessionID: string
}

export interface UserTx {
    tx: db.Transaction;
    ownerId: string;
}

export interface TransactRequest extends AuthRequest {
    userTx:UserTx
}

export import Response = express.Response;

/* istanbul ignore next */ 
export function autoAnswer(res:Response) {
    return function(d:any) {
        if (d != null) res.answer(d);
    }
}

/* istanbul ignore next */  
export function wrapTx(mw:(req:TransactRequest, res:Response) => Promise<any>) {
    return function(req:AuthRequest, res: express.Response) {
        var aa = autoAnswer(res);
        db.transaction(tx => {
            // tx.logQueries(true);
            var treq = <TransactRequest>req;
            treq.userTx = { tx: tx, ownerId: null };
            return mw(treq, res);
        }).done(aa, aa);
    }
}