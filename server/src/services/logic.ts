import stream = require("stream");
import Promise = require('bluebird');

import env = require('../lib/env-utils');
import Service = require("../lib/service");
import codedError = require('../lib/coded-error');

import config = require("../config/index");
/* istanbul ignore next */ 
class Logic extends Service {
    constructor() { super(config.services.logic.url); }
    postEvent(eventName:string, eventData:any, sync=false) {
        if (env.isTest()) return Promise.resolve({ done: true });
        return this.post<{done:boolean}>('/event/', { eventName: eventName, eventData: eventData, sync: sync })
            .then(Service.extractBody);
    }
}
/* istanbul ignore next */
export function create() { return new Logic(); }