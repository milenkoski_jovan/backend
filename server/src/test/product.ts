import tt = require('./lib/test');
import db = require('../lib/base');
import uuid = require('uuid');
import Promise = require('bluebird');
import _ = require('lodash');
import TM = require('../products/product-manager');

function withPM(f: (tm: TM.ProductManager) => Promise<any>) { return db.transaction(tx => f(TM.create({ tx: tx }))); }

tt.test("Product Manager", t => {
    var product:any = {
        name: 'dinner',
        action: false
    }
});