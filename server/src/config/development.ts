// add development configuration here (we would need to replace it for hooray)
exports.baseAddress = "http://localhost:8080";

exports.domain = "hooray";

exports.loggerType = 'file';
exports.logFilePath = require('path').resolve(__dirname, '../logs.txt');

exports.defaultGuiLanguage = 'en';

exports.registrationWhitelist = [
    /.*/
];

exports.database = {
    url: 'mysql://root:Supermen!1@mojmarket.crswlvoekjdg.eu-west-1.rds.amazonaws.com:3306/mojmarket',
    connections: { min: 1, max: 4 }
};