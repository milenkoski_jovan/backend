//-----------------------------------------------------------------------
// add common (default) configuration below
//-----------------------------------------------------------------------

exports.sessionSecret = 'neDSFsioV$%nfCewan83';

exports.baseAddress = process.env['NODE_ENV'] == 'production' ? "http://www.hooray.com" : "http://localhost:8080";

exports.emailDomain = "mailer.hooray.com";
exports.emailName = "Hooray";
exports.emailUsername = "no-reply";

exports.appName = 'Hooray';
exports.emailTemplatesDir = 'default';

exports.defaultMaxItems = 100;

exports.services = {
    logic: { url: "https://logicservice.hoorray-test-platform.com" },
    importio: { url: 'https://api.import.io'}
};

export const maxListLengthInWhere = 300;