import _ = require('lodash')
import crypto = require('crypto');
import Promise = require('bluebird');
import codedError = require('../lib/coded-error');
import config = require('../config/index');
import Transaction = require('../lib/transaction-like');
import Transactionable = require('../lib/transactionable');
import UserTransactionable = require('../lib/user-transactionable');
import uuid = require('uuid');
import db = require('../lib/base');

import { Product, Market } from '../entities/all';
import { ProductWithMarket } from './interfaces';

export class ProductManager extends UserTransactionable {

    getAllProducts(): Promise<ProductWithMarket[]> {
        return Product.from(Product.join(Market).on(Product.marketId.equals(Market.id)))
            .select<ProductWithMarket>(
                Product.star(), 
                Market.name.as("marketName"),
                Market.imgUrl.as("marketImgUrl")
             )
            .allWithin(this.tx).then(products => {
                return _.uniq(products, 'name');
            })
    }

    getAllMarkets(): Promise<Market[]> {
        return Market.select<Market>(Market.star()).allWithin(this.tx);
    }

    getMarketById(marketId: string): Promise<Market> {
        return Market.select<Market>(Market.star())
            .where(Market.id.equals(marketId)).getWithin(this.tx);
    }

    getProductsByMarket(marketId: string): Promise<Product[]> {
        return Product.select<Product>(Product.star())
            .where(Product.marketId.equals(marketId)).allWithin(this.tx);
    }

    getProductsByName(productName: string): Promise<Product[]> {
         return Product.from(Product.join(Market).on(Product.marketId.equals(Market.id)))
            .select<ProductWithMarket>(
                Product.star(), 
                Market.name.as("marketName"),
                Market.imgUrl.as("marketImgUrl")
             )
            .where(Product.name.equals(productName))
            .allWithin(this.tx);
    }

}

export function create(opt: { tx: Transaction; ownerId?: string }) {
    return new ProductManager(opt);
}