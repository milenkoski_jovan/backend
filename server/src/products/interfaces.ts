 import { Product, Market } from '../entities/all';
 
 export interface ProductWithMarket extends Product{
     market: Market
 }