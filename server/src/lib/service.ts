import bl = require('bl');
import _ = require('lodash');
import http = require('http');
import https = require('https');
import stream = require('stream');
import qs = require('querystring');
import Promise = require('bluebird');
import through = require('through2');
import duplexer = require('duplexer2');

import strUtils = require('./string-utils');

/* istanbul ignore next */ // This never happens, but feel free to add it back if it does.
function replaceAll(str:string, context:Dictionary<string>) {
    var reg = /:([^\/]+)/;
    var match:RegExpMatchArray;
    var names:Dictionary<boolean> = {};
    while ((match = str.match(reg))) {
        var name = match[1], inner = match[0];
        str = str.substr(0, match.index) + context[name] + str.substr(match.index + inner.length);
        names[name] = true;
    }
    return str;
}

interface WithBody<T> { body:T }

interface ThenableDuplexer<T> extends NodeJS.ReadWriteStream {
    then:(succ:(value:WithBody<T>)=>any,err?:(error:any)=>any)=>Promise<any>
    expect:(code:number)=>ThenableDuplexer<T>
    promise:()=>Promise<WithBody<T>>
}

interface NewRequestOptions {
    url?:string
    method?:string
    body?:Buffer
    json?:boolean
    query?:Dictionary<string>
    headers:Dictionary<any>
}

interface RequestOptions {
    url:string
    query:Dictionary<string>
    method:string
    headers:Dictionary<any>
    body:any
    json?:boolean
}

/* istanbul ignore next */
class Service {
    private protocol:string;
    private hostname:string;
    private defaultPath:string;
    private port:number;

    constructor(private baseUrl:string) {
        var parts = baseUrl.split("/");
        this.protocol = parts[0].substring(0, parts[0].length - 1);
        var parts2 = parts[2].split(":");
        this.hostname = parts2[0];
        if (parts2[1]) this.port = parseInt(parts2[1]);
        this.defaultPath = parts.slice(3).join("/");
        if (!strUtils.startsWith(this.defaultPath, "/")) this.defaultPath = "/" + this.defaultPath;
        console.log("Service wrapper initialized", baseUrl);
    }

    protected getDefaultOpt(headers:Dictionary<string>={}):NewRequestOptions {
        return { headers: _.defaults<Dictionary<string>, Dictionary<string>>(headers, {
            'X-Requested-With': "XMLHttpRequest"
        }) };
    }

    private request<T>(opt:RequestOptions) {
        console.log('opt request', opt);
        var p = Promise.defer();
        var expectedCode = 200;

        var path = replaceAll(opt.url, opt.query);
        if (strUtils.startsWith(path, "/") && strUtils.endsWith(this.defaultPath, "/"))
            path = this.defaultPath.substring(0, this.defaultPath.length - 1) + path;
        else path = this.defaultPath + path;
        var reqOptions = {
            hostname: this.hostname,
            method: opt.method,
            path: path,
            port: this.port,
            headers: opt.headers || {}
        };
        console.log('req options', reqOptions);

        var postdata:Buffer = undefined;
        if (opt.body != null) {
            postdata = new Buffer(JSON.stringify(opt.body));
            reqOptions.headers['Content-Type'] = 'application/json';
            reqOptions.headers['Content-Length'] = postdata.length;
        }

        if (opt.query != null)
            reqOptions.path += '?' + qs.stringify(opt.query);

        var inp = through(), out = through();
        function handleRequest(res:any) { // TODO deanify
            res.on('error', p.reject.bind(p));
            res.pipe(new bl((err:Error, buf:Buffer) => {
                if (err)
                    return p.reject(err);
                var unparsed = res.body = buf.toString();
                if (opt.json)
                    try  {
                        if (res.body.indexOf("Cannot " + reqOptions.method.toUpperCase()) == 0)
                            err = new Error("Missing endpoint: " + reqOptions.path);
                        else res.body = JSON.parse(res.body);
                    } catch (e) {
                        err = new Error(res.body);
                    }
                if (res.statusCode != expectedCode) {
                    if (!err) {
                        var msg = res.body.stack || unparsed;
                        err = new Error("HTTP code " + res.code + ' ' + msg);
                    }
                }
                p.callback(err, res);
            }));
            res.pipe(out);
        }
        var request:Function;
        switch (this.protocol.toLowerCase()) {
            case "http": request = http.request; break;
            case "https": request = https.request; break;
            default: throw new Error("Unknown protocol: " + this.protocol);
        }
        var req = request(reqOptions, handleRequest);
        req.on('error', p.reject.bind(p));

        if (opt.body != null || opt.method == 'GET' || opt.method == 'DELETE')
            req.end(postdata);
        else
            inp.pipe(req);
        var ret = <ThenableDuplexer<T>>duplexer(inp, out);
        ret.then = (succ, err) => p.promise.then(succ, err);
        function expect(code:number) {
            expectedCode = code;
            return ret;
        }
        ret.expect = expect;
        ret.promise = () => <Promise<WithBody<T>>>p.promise;
        return ret;
    }

    private _get<T>(url:string, query:Dictionary<string>={}, opt:NewRequestOptions=this.getDefaultOpt()) {
        opt.url = url;
        opt.method = 'GET';
        opt.query = query || {};
        console.log("opt", opt);
        return this.request<T>(<RequestOptions>opt);
    }
    private _post<T>(url:string, body?:Buffer, opt:NewRequestOptions=this.getDefaultOpt()) {
        opt.url = url;
        opt.method = 'POST';
        opt.body = body;
        return this.request<T>(<RequestOptions>opt);
    }

    private _delete<T>(url:string, opt:NewRequestOptions=this.getDefaultOpt()) {
        opt.url = url;
        opt.method = 'DELETE';
        return this.request<T>(<RequestOptions>opt);
    }

    private getJSON<T>(url:string, query:Dictionary<string>={}, opt:NewRequestOptions=this.getDefaultOpt()) {
        console.log('get json url: ', url);
        opt.json = true;
        return this._get<T>(url, query, opt);
    }

    private postJSON<T>(url:string, body:Buffer, opt:NewRequestOptions=this.getDefaultOpt()) {
        opt.json = true;
        return this._post<T>(url, body, opt);
    }

    private deleteJSON<T>(url:string, opt:NewRequestOptions=this.getDefaultOpt()) {
        opt.json = true;
        return this._delete<T>(url, opt);
    }

    post<T>(url:string, what:any, headers:Dictionary<string>={}) {
        return this.postJSON<T>(url, what, this.getDefaultOpt(headers));
    }

    get<T>(url:string, qs?:Dictionary<any>, headers:Dictionary<string>={}) {
        console.log('url: ', url);
        return this.getJSON<T>(url, qs, this.getDefaultOpt(headers));
    }

    delete<T>(url:string, headers:Dictionary<string>={}) {
        return this.deleteJSON<T>(url, this.getDefaultOpt(headers));
    }

    postRaw(url:string, what?:any) { return this._post<string>(url, what, this.getDefaultOpt()); }

    getRaw(url:string, qs?:Dictionary<string>) { return this._get<string>(url, qs, this.getDefaultOpt()); }

    static extractBody<T>(response:WithBody<T>):T { return response.body; }
}

export = Service;