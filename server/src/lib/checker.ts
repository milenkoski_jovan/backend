import codedError = require('../lib/coded-error');
import _ = require('lodash')

/* istanbul ignore next */ 
export enum Type {
    String = <any>"string",
    Number = <any>"number",
    Object = <any>"object"
}
/* istanbul ignore next */ 
export enum Qualifier {
  MoreThan = <any>"moreThan",   
  LessThan = <any>"lessThan",
  Equals = <any>"equals",
  NotEquals = <any>"notEquals",
  NotEmpty = <any>"notEmpty",   
  NoQualifier = <any>"noQualifier"
}
/* istanbul ignore next */ 
export function check(value: any, type: Type, 
    criteria?: {    
        length?: number; 
        value?: number, 
        qualifier: Qualifier},
    errorMsg?:string) {

    if (!value) throw codedError(400, errorMsg || "Invalid value")

    switch (type)
    {
        case Type.String: {
                // TODO: Implement case content
                handleStringValue(value, type, criteria, errorMsg);
            break;
        }
        case Type.Number: {
                handleNumberValue(value, type, criteria, errorMsg);
            break;
        }
        case Type.Object: {
            if (criteria.qualifier == Qualifier.NotEmpty) {
                if (_.isEmpty(value))
                    throw codedError(400, errorMsg || 'Invalid object - object is empty')
            }
            break;
        }
        default: {
            // TODO: Implemente default case
            throw codedError(400, 'Type not provided');
        }
    }
}
/* istanbul ignore next */ 
function handleStringValue(value: any, type: Type, 
    criteria?: {    
        length?: number; 
        value?: number, 
        qualifier: Qualifier},
    errorMsg?:string) {
        if (typeof(value) !== "string")
            throw codedError(400, errorMsg || "Invalid value - not a string");
        
        var qualifier = criteria.qualifier;

        if (criteria) {
            if (criteria.value) {
                if (qualifier == Qualifier.Equals) {
                    if (value !== criteria.value) {
                        throw codedError(400, errorMsg || "Invalid value - strings are not equal")
                    }
                } else if (qualifier == Qualifier.NotEquals) {
                    if (value == criteria.value) {
                        throw codedError(400, errorMsg || 'Invalud value - strings are equal');
                    }
                }
            }

            if (criteria.length) {
                if (qualifier == Qualifier.Equals) {
                    if (value.length !== criteria.length) {
                        throw codedError(400, errorMsg || "Invalid value - string has invalid length")
                    }

                } else if (qualifier == Qualifier.NotEquals) {
                    if (value.length !== criteria.length) {
                        throw codedError(400, errorMsg || "Invalud value - String has invalid length")
                    }
                }
            }

            if (criteria.length) {
                throw codedError(400, 'Cannot check length of number')
            }
        }
}
/* istanbul ignore next */ 
function handleNumberValue(value: any, type: Type, 
    criteria?: {    
        length?: number; 
        value?: number, 
        qualifier: Qualifier},
    errorMsg?:string) {
        if (typeof(value) !== "number")
            throw codedError(400, errorMsg || "Invalid value - not a number");

        if (criteria) {

            var qualifier = Qualifier.Equals;

            if (criteria.qualifier) 
                qualifier = criteria.qualifier;

            if (criteria.value) {

                if (qualifier == Qualifier.Equals) {

                } else if (qualifier == Qualifier.MoreThan) {

                } else if (qualifier == Qualifier.LessThan) {

                } 

            }

            if (criteria.length) {
                throw codedError(400, 'Cannot check length of number')
            }
        }
}

