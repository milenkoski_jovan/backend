import Promise = require('bluebird');

interface QueryLike {
    query:string;
    values: any[]
    text:string
}

interface Transaction {
    queryAsync<T>(query:string, ...params:any[]):Promise<{rowCount:number;rows:T[]}>;
    queryAsync<T>(query:QueryLike):Promise<{rowCount:number;rows:T[]}>
}

export = Transaction;