import Promise = require('bluebird');

/**
 * Like Bluebird's {@link Promise.map()}, only the promises are resolved in sequence instead of in parallel.
 * @param {Array} items an array of items to map.
 * @param {function<*>} mapFn the mapping function.
 * @returns {Promise<Array>} resolves to an array of the results of the mapping function applied to each item.
 */
/* istanbul ignore next */  
export function mapSeries<T, U>(items:T[], mapFn:(item:T, index?:number)=>Promise<U>):Promise<U[]>
/* istanbul ignore next */  
export function mapSeries<T, U>(items:T[], mapFn:(item:T, index?:number)=>U):Promise<U[]> {
    var current:Promise<any> = Promise.resolve();

    if (!items) throw new Error("Items array not provided");
    if (!items.map) throw new TypeError("Items are not an array: " + items);

    var results = items.map(function(item, index) {
        return (current = current.then(_ => mapFn(item, index)));
    });
    return Promise.all(results);
}

/**
 * Nested version of {@link mapSeries}.
 * @param {Array} items an array of items to map.
 * @param {...Function} fn mapping functions. The first function in the series maps <code>items</code>, while each
 *                         following function takes the result from the preceding function and maps it to another value.
 *                         The final function is the one that is executed. If only one function is provided, this is (or
 *                         at least should be) equivalent to calling <code>mapSeries</code>.
 * @returns {Promise<Array>} resolves to an array of the results of the mapping function applied to each item.
 */
/* istanbul ignore next */  
export function mapSeriesNested<T, U>(items:T[], fn:(item:T)=>any, ...restOfFunctions:Function[]):Promise<U[]> {
    if (arguments.length <= 2) return <Promise<U[]>>mapSeries(items, fn);

    var current = Promise.resolve();
    var results:U[] = [];
    items.map(fn).forEach(function (mappableArray) {
        current = current.then(function () {
            return mapSeriesNested.apply(null, [mappableArray].concat(restOfFunctions));
        }).then(function (res:U[]) { results = results.concat(res); });
    });

    return current.then(function() { return results; });
}


/**
 * Like Bluebird's {@link Promise.props()}, only the promises are resolved in sequence instead of in parallel.
 * @param {Object} items the object to resolve.
 * @returns {Promise<Object>} resolves to an object matching the resolved properties of <code>items</code>.
 */
/* istanbul ignore next */  
export function propsSeries<T>(items:Dictionary<T>):Promise<Object> {
    var current = Promise.resolve();
    var results:Dictionary<any> = {};

    for (var key in items) if (items.hasOwnProperty(key)) (function(key:string) {
        current = current.return(items[key]).then(function(result:any) { results[key] = result; })
    })(key);

    return <Promise<Object>>current.then(() => results);
}

/**
 * Like Bluebird's {@link Promise.any()}, only returns a <code>true</code> or <code>false</code> value depending on
 * whether any of the specified were fulfilled.
 * @param items a list of promises to look over.
 * @returns {Promise<boolean>}
 */
/* istanbul ignore next */  
export function anyTrueFalse(items:Promise<any>[]) {
    return Promise.any(items).then(() => true).error(function(err) {
        if ((err.name == 'AggregateError') && (err.length == items.length)) return false;
        throw err;
    });
}