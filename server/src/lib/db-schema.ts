// Works for SQLite in testing and MySQL in all other environments.
import url = require('url');
import _ = require('lodash');
import Promise = require('bluebird');

import env = require('./env-utils');
import Transaction = require('./transaction-like');

import config = require('../config/index');

import { Column } from 'anydb-sql';

function getDatabaseUrl():string { return config.database.url; }

/**
 * Gets the name of the database for the current configuration.
 * @return {String} the name of the database for the current configuration.
 */
export function getDatabaseName():string { return _.last<string>(getDatabaseUrl().split('/')); }

/** Gets the database dialect. */
export function extractDialect() {
    var dialect = url.parse(getDatabaseUrl()).protocol;
    dialect = dialect.substr(0, dialect.length - 1);
    if (dialect == 'sqlite3')
        dialect = 'sqlite';
    return dialect;
}

/**
 * Checks if a column exists in a table in the current database.
 * @param tx the transaction to use to make the check query.
 * @param tableName the name of the table.
 * @param columnName the name of the column.
 * @return {Promise<boolean>} resolves to a value indicating whether the specified column exists.
 */
export function columnExistsByName(tx:Transaction, tableName:string, columnName:string):Promise<boolean> {
    if (env.isTest()) { // SQLite
        return tx.queryAsync<{name:string}>('PRAGMA table_info(' + tableName + ');').then(res => {
            return _.any(res.rows, row => row.name == columnName);
        });
    } else { // MySQL
        return tx.queryAsync(
                "SELECT * " +
                "FROM information_schema.COLUMNS " +
                "WHERE TABLE_SCHEMA = '" + getDatabaseName() + "' AND " +
                "TABLE_NAME = '" + tableName + "' AND " +
                "COLUMN_NAME = '" + columnName + "'"
        ).then(res => res.rowCount > 0);
    }
}

/**
 * Checks if a column exists in the current database.
 * @param tx the transaction to use to make the check query.
 * @param column the column to check.
 * @return {Promise<boolean>} resolves to a value indicating whether the specified column exists.
 */
export function columnExists(tx:Transaction, column:Column<any>):Promise<boolean> {
    return columnExistsByName(tx, (<any>column).table._name, (<any>column).name);
}