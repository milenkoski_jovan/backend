/**
 * Created by spion on 11/13/14.
 */

import Transaction = require('./transaction-like');
/* istanbul ignore next */ 
class Event<T> {
    constructor(public tx:Transaction, public data:T) {}

    /* istanbul ignore next */ // Not testing this, seems to be unused.
    public new<U>(data:U):Event<U> {
        return new Event(this.tx, data);
    }
}

export = Event;