import Promise = require('bluebird');

var entities = require('../entities/all');

import { Transaction, Table } from 'anydb-sql';

function getModels():Table<any>[] {
    return Object.keys(entities).map(modelName => entities[modelName]).filter(table => {
        return (typeof table.create == "function") && (typeof table.drop == "function");
    });
}

export function up(tx:Transaction) {
    return Promise.map(getModels(), table => table.create().ifNotExists().execWithin(tx));
}

export function down(tx:Transaction) {
    return Promise.map(getModels(), table => table.drop().ifExists().execWithin(tx));
}