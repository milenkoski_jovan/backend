import env = require('../lib/env-utils');
import Logic = require('../services/logic');
import config = require('../config/index');
/* istanbul ignore next */
class EventManager {
    private logicService = Logic.create();

    /**
     * Asynchronously posts an event.
     * @param eventName the name of the event.
     * @param data additional event data.
     */
    postEvent(eventName:string, data:any) { return this.logicService.postEvent(eventName, data, true); }
}

export = EventManager