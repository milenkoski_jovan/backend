import db = require('../lib/base');

import { Column, Table } from 'anydb-sql';

const TYPE_UUID = "CHAR(36)",
      TYPE_USER_ID = "VARCHAR(50)"
;
      
export interface Market {
    id: string
    name:string
    imgUrl: string
    map: string
}
export interface MarketTable extends Table<Market> {
    id: Column<string>
    name: Column<string>
    imgUrl: Column<string>
    map: Column<string>
}
export var Market = <MarketTable>db.define<Market>({
    name: 'Market',
    columns: {
        id: { primaryKey: true, notNull: true , dataType: TYPE_UUID },
        name: { dataType: "CHAR(20)" },
        map: { dataType: "TEXT" },
        imgUrl: { dataType: "TEXT" }
    }
});