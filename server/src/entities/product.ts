import db = require('../lib/base');

import { Column, Table } from 'anydb-sql';

const TYPE_UUID = "CHAR(36)",
      TYPE_USER_ID = "VARCHAR(50)"
;
      
export interface Product {
    id: string
    marketId: string
    name:string
    img: string
    action: boolean
}

export interface ProductTable extends Table<Product> {
    id: Column<string>
    name:Column<string>
    action: Column<boolean>
    img: Column<string>
    marketId: Column<string>
}

export var Product = <ProductTable>db.define<Product>({
    name: 'Product',
    columns: {
        id: { primaryKey: true, notNull: true , dataType: TYPE_UUID },
        name: { dataType: "CHAR(20)" },
        img: { dataType: "TEXT" },
        marketId: { dataType: TYPE_UUID }, 
        action: { dataType: "BOOLEAN" }
    }
});