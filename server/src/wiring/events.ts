import Event = require('../lib/event');

import EventManager = require('../events/event-manager');

// import FM = require('../versions/file-manager');
/* istanbul ignore next */
function postEvent<T>(eventName:string, event:Event<T>) { return new EventManager().postEvent(eventName, event.data); }
/* istanbul ignore next */
function getHandler<T>(eventName:string, async=true) {
    return async ? (e:Event<T>) => { postEvent(eventName, e); } : (e:Event<T>) => postEvent(eventName, e);
}