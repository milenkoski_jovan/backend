import fs = require('fs');

export function initialize() {
    fs.readdirSync(__dirname).forEach(function(filename) {
        if (!/^index[.]/.test(filename) &&
            /[.]js$/.test(filename)) require('./' + filename);
    });
}