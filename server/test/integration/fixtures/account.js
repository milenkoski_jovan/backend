var user = require('./user');

exports.baseAccountId     = '5d8b39f5-d6f0-4b7c-9292-23bcec07924';
exports.baseUserAccountId = '5d8b39f5-d6f0-4b7c-9292-1111111113'; //xx

exports.getUserAccountObject = function (opt) {
    if (typeof(opt.aid) !== "number")
        throw new TypeError("Must provide account id number");
    if (typeof(opt.uid) !== "number")
        throw new TypeError("Must provide user id number");
    return {
        id: exports.baseUserAccountId + opt.uid + opt.aid,
        userId: user.baseuid + opt.uid,
        accountId: exports.baseAccountId + opt.aid,
        username: 'user ' + opt.uid + ' ' + opt.aid,
        displayName: 'user ' + opt.uid + ' ' + opt.aid
    }
};


exports.getAccountObject = function (opt) {
    if (typeof(opt.aid) !== "number")
        throw new TypeError("Must provide account id number");
    var type = opt.accountType || 'free';
    return {
        id: exports.baseAccountId + opt.aid,
        name: 'Company ' + opt.aid,
        type: type,
        service: 'mock',
        credentials: null,
        isShared: type == 'business',
        creatorId: user.baseuid + opt.aid
    };
};

/**
 * Generates a fake DoxBee auth object.
 * @param {Object} opt
 * @param {Number} opt.aid account ID number used to generate a fake account, <code>0</code>-<code>9</code>.
 * @param {Number} opt.uid user ID number used to generate a fake user, <code>0</code>-<code>9</code>.
 * @param {string} [opt.accountType='free'] can be <code>'free'</code> or <code>'business'</code>.
 * @returns {{Accounts:Object,UserAccounts:Object}}
 */
exports.getAuthObject = function (opt) {
    var uobj = user.getInfo(opt);
    uobj.Accounts = exports.getAccountObject(opt);
    uobj.UserAccounts = exports.getUserAccountObject(opt);
    return uobj;
};