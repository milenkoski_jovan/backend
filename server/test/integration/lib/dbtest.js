Error.stackTraceLimit = 30;
process.env.NODE_ENV = 'test';
require('source-map-support').install();
var P = require('bluebird');
P.longStackTraces();


var t = require('blue-tape');
var mwApp = require('../../../dst/hooray.js');
var server = require('./server');

var path = require('path');

var entities = require('../../../dst/entities/all');
var models = Object.keys(entities).map(function(modelName) { return entities[modelName]; });

function deleteAll() {
    return P.all(models.map(function (model) {
        var p = P.resolve(null);
        if (model.create && model.create().exec) {
            try {
                p = model.create().ifNotExists().all();
            } catch (e) {}
        }
        p = p.then(function() {
            if (model.delete && model.delete().exec)
                return model.delete().all();
            else
                return true;
        });
        return p;
    }));
}


exports.before = function(t) {
//    db.open();
    return P.join(deleteAll(), server.init(mwApp))
};
exports.after = function(t) {
//    db.close();
    server.get().close();
    t.end();
};


exports.delay = function(time) {
    return new P(function(fulfill) {
        setTimeout(fulfill, time);
    });
};

exports.test = function() {
    t.test('db-open', exports.before);
    t.test.apply(t, arguments);
    t.test('db-close', exports.after);
};

exports.errorTest = function errorTest(t, action, message) {
    return P.try(function() { return action.then(function() { t.notOk(true, message); }); }).catch(function(err) {
        t.ok(err, message);
    });
};