var _ = require('hidash');
var Promise = require('bluebird');
var bl = require('bl');
var ffile = require('../fixtures/file');

function returnBody(res) { return res.body; }

/**
 * Uploads a fake file.
 * @param {User} user the user to upload the file as.
 * @param {String} filename the name of the file.
 * @param [fileContent='Hello&nbsp;world\n'] the contents of the file.
 * @returns {Promise<Object>} a promise that resolves to the uploaded file.
 */
exports.uploadFakeFile = function uploadFakeFile(user, filename, fileContent) {
    return exports.uploadFile(user, ffile.fakeFile({ content: fileContent }), filename);
};

exports.uploadFile = function uploadFile(user, stream, filename) {
    return stream.pipe(user.post('/files/create/' + filename, { ownerId: user.ownerId })).then(function() {
        return exports.getFile(user, filename);
    });
};

/**
 * Convenience function that uploads a fake file while making sure to download the current version of the file so the
 * upload won't be rejected.
 * @param {User} user the user to act as.
 * @param {String} filename the name of the file.
 * @param [fileContent='Hello&nbsp;world\n'] the contents of the file to upload.
 * @returns {Promise<Object>} a promise that resolves to the uploaded file.
 */
exports.downloadAndUpload = function downloadAndUpload(user, filename, fileContent) {
    return Promise.resolve(exports.download(user, filename)).catch(function(err) {
        console.log("Unable to download requested file, probably doesn't exist yet", filename);
    }).then(function() {
        return exports.uploadFakeFile(user, filename, fileContent);
    });
};

/**
 * Uploads a fake file as a guest.
 * @param {Guest} guest the guest to upload the file as.
 * @param [fileContent='Hello&nbsp;world\n'] the contents of the file.
 * @returns {Promise<Object>}
 */
exports.uploadFakeFileAsGuest = function uploadFakeFileAsGuest(guest, fileContent) {
    return ffile.fakeFile({ content: fileContent }).pipe(guest.post('/files/upload/$guest')).then(returnBody);
};

/**
 * Gets a file.
 * @param {User} user the user to get the file.
 * @param {String|uuid} filename the name or ID of the file to get.
 * @returns {Promise<Object>} a promise that resolves to the file.
 */
exports.getFile = function getFile(user, filename) { return user.get('/files/get/' + filename, { ownerId: user.ownerId }).then(returnBody); };

/**
 * Saves a file.
 * @param {User} user the user to save the file.
 * @param {String} filename the path to the file.
 * @param {String} versionId the ID of the version to save.
 * @param {String} [tag] a tag for the saved version.
 * @returns {Promise<{file:Object,version:Object}>} resolves to a promise that contains the saved file and new version.
 */
exports.saveAs = function saveAs(user, filename, versionId, tag) {
    return user.post('/files/saveas/' + filename, { ownerId: user.ownerId, versionId: versionId, tag: tag }).then(returnBody);
};

/**
 * Resolves a file.
 * @param {User} user the user to resolve the file.
 * @param {String} filename the name of the file.
 * @param {String} versionId the ID of the version to resolve.
 * @param {String} resolutionType <code>'accept'</code>, <code>'reject'</code>, <code>merge</code>
 * @param {String} [tag] a tag for the resolved version.
 * @param {String} [blobId] blob id for a merged version
 * @returns {Promise<{file:Object,version:Object}>} resolves to a promise that contains the resolved file and new
 *                                                  version.
 */
exports.resolve = function resolve(user, filename, versionId, resolutionType, tag, blobId) {
    return user.post('/files/resolve/' + filename, { ownerId: user.ownerId, versionId: versionId, type: resolutionType, tag: tag, blobId: blobId })
        .then(returnBody);
};

/**
 * Downloads a file.
 * @param {User} user the user to download the file.
 * @param {String} filename the path to the file.
 * @param {String} [versionId] the version of the file to get. If omitted, gets the current version.
 * @returns {Stream}
 */
exports.download = function download(user, filename, versionId) {
    return user.get('/files/download/' + filename, { ownerId: user.ownerId, revision: versionId });
};

/**
 * Deletes a file.
 * @param {User} user the user to delete the file.
 * @param {String} path the path to the file.
 * @returns {Promise<{done:boolean}>} resolves to <code>{ done: true }</code>.
 */
exports.deleteFile = function deleteFile(user, path) {
    return user.post('/files/delete', { ownerId: user.ownerId, path: path }).then(returnBody);
};

/**
 * Combines several revisions of a file. This should only be used as a file resolution method.
 * @param {User} user the user performing the action.
 * @param {String} path the path to the file.
 * @param {String} baseVersionId the ID of the version being resolved.
 * @param {String[]} versionIds a list of IDs specifying the version to combine. This list should include the base
 *                              version (<code>baseVersionId</code>).
 * @returns {Promise}
 */
exports.combine = function combine(user, path, baseVersionId, versionIds) {
    if (!_.contains(versionIds, baseVersionId))
        console.log("Warning: combine base version (" + baseVersionId + ") not present in version ID list", versionIds);
    return user.post('/files/combine/' + path, { ownerId: user.ownerId, baseId: baseVersionId, revIds: versionIds }).then(returnBody);
};

/**
 * Copies a file.
 * @param {User} user the user that does the copying.
 * @param {string|uuid} origin the name or ID (I think) of the origin file.
 * @param {string} destination the destination folder (<code>"/"</code> for root).
 * @param {string} copyName the name of the copied file.
 * @returns {Promise<{done:boolean}>} resolves to <code>{ done: true }</code>.
 */
exports.copy = function copy(user, origin, destination, copyName) {
    return user.post('/files/copy', { ownerId: user.ownerId, origin: origin, dest: destination, name: copyName }).then(returnBody);
}

exports.init = function init(user) {
    return user.post('/files/init', { ownerId: user.ownerId }).then(returnBody);
}

/**
 * Creates a promise stream that collects data from another stream into a buffer
 * @returns a stream with a method promise returning a promise for the buffer
 */
exports.promiseStream = function promiseStream() {
    var p = Promise.pending();
    var s = bl(function (err, res) {
        if (err) p.reject(err);
        else p.fulfill(res)
    });
    s.promise = function () {
        return p.promise;
    };
    return s;
};