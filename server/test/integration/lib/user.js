var Promise = require('bluebird');

var fuser = require('../fixtures/user');
var faccount = require('../fixtures/account');

/**
 * @param {Object} opt
 * @param {Boolean} [opt.auth=false] a value indicating whether this instance should have an auth object.
 * @param {Number} opt.aid account ID number used to generate a fake account, <code>0</code>-<code>9</code>. Required if
 *                         <code>opt.auth</code>.
 * @param {Number} opt.uid user ID number used to generate a fake user, <code>0</code>-<code>9</code>. Required if
 *                         <code>opt.auth</code>.
 * @constructor
 * @throws TypeError if <code>opt.auth</code> but no <code>opt.aid</code> and <code>opt.uid</code>.
 */
function User(opt) {
    this.opt = opt;
    this.app = require('./server').get();
    if (opt.auth) {
        this.auth = faccount.getAuthObject(opt);
    }
}

User.prototype.serverAddress = function() {
    return this.app.address();
};

User.prototype.request = function() {
    if (!this.auth)
        return this.app.tester({session:{}});
    else
        return this.app.tester({session:{user:this.auth}});

};

User.prototype.post = function(url, data, opt) {
    opt = opt || {};
    opt.headers = opt.headers || {};
    opt.headers['X-Requested-With'] = "XMLHttpRequest";
    return this.request().postJSON(url, data, opt);
};

User.prototype.postRaw = function(url, data, opt) {
    opt = opt || {};
    opt.headers = opt.headers || {};
    opt.headers['X-Requested-With'] = "XMLHttpRequest";
    return this.request().post(url, data, opt);
};

User.prototype.get = function(url, data, opt) {
    if (!url) throw new Error("Missing URL");

    opt = opt || {};
    opt.headers = opt.headers || {};
    opt.headers['X-Requested-With'] = "XMLHttpRequest";
    return this.request().getJSON(url, data, opt);
};

User.prototype.getRaw = function(url, data, opt) {
    opt = opt || {};
    opt.headers = opt.headers || {};
    opt.headers['X-Requested-With'] = "XMLHttpRequest";
    return this.request().get(url, data, opt);
};

User.prototype.delete = function(url, data, opt) {
    opt = opt || {};
    opt.headers = opt.headers || {};
    opt.headers['X-Requested-With'] = "XMLHttpRequest";
    return this.request().deleteJSON(url, data, opt);
};

User.prototype.getInfo = function() {
    return fuser.getInfo(this.opt);
};

User.prototype.getEmailPass = function() {
    return fuser.getEmailPass(this.opt);
};

User.prototype.getAuthObject = function() {
    return this.auth;
};

/**
 * Generates a new fake user.
 * @param {Object} opt
 * @param {Boolean} opt.auth a value indicating whether this instance should have an auth object.
 * @param {Number} opt.aid account ID number used to generate a fake account, <code>0</code>-<code>9</code>. Required if
 *                         <code>opt.auth</code>.
 * @param {Number} opt.uid user ID number used to generate a fake user, <code>0</code>-<code>9</code>. Required if
 *                         <code>opt.auth</code>.
 * @constructor
 * @throws TypeError if <code>opt.auth</code> but no <code>opt.aid</code> and <code>opt.uid</code>.
 */
module.exports = function(opt) {
    return new User(opt);
};
