// add test configuration here
console.log('exporting test config');
exports.baseAddress = "http://localhost:8500";
exports.domain = "hooray";
exports.mockSession = true;
exports.loggerType = 'file';
exports.logFilePath = require('path').resolve(__dirname, '../logs.txt');
exports.database = {
    url: 'sqlite://',
    connections: { min: 1, max: 4 }
};
//# sourceMappingURL=test.js.map