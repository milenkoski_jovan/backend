"use strict";
var _ = require('lodash');
//-----------------------------------------------------------------------
// Assume development env.                                              -
//-----------------------------------------------------------------------
/* istanbul ignore if */
if (!process.env['NODE_ENV'])
    process.env['NODE_ENV'] = 'development';
/* istanbul ignore next */ // probably not the point to test this
function overrideArrays(a, b) {
    if (b instanceof Array)
        return b;
}
var config = {};
try {
    _.merge(config, require('./common'), overrideArrays);
}
catch (e) {
    /* istanbul ignore next */
    console.warn("Common configuration not found, skipping...", e);
}
var which = process.env['NODE_ENV'] == 'production' ? './production' :
    process.env['NODE_ENV'] == 'test' ? './test' :
        './development';
try {
    _.merge(config, require(which), overrideArrays);
}
catch (e) {
    /* istanbul ignore next */
    console.warn("Specific configuration ", which, " not found, skipping...");
}
module.exports = config;
//# sourceMappingURL=index.js.map