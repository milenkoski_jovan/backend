"use strict";
var EventManager = require('../events/event-manager');
// import FM = require('../versions/file-manager');
/* istanbul ignore next */
function postEvent(eventName, event) { return new EventManager().postEvent(eventName, event.data); }
/* istanbul ignore next */
function getHandler(eventName, async) {
    if (async === void 0) { async = true; }
    return async ? function (e) { postEvent(eventName, e); } : function (e) { return postEvent(eventName, e); };
}
//# sourceMappingURL=events.js.map