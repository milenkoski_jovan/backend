"use strict";
var fs = require('fs');
function initialize() {
    fs.readdirSync(__dirname).forEach(function (filename) {
        if (!/^index[.]/.test(filename) &&
            /[.]js$/.test(filename))
            require('./' + filename);
    });
}
exports.initialize = initialize;
//# sourceMappingURL=index.js.map