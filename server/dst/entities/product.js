"use strict";
var db = require('../lib/base');
var TYPE_UUID = "CHAR(36)", TYPE_USER_ID = "VARCHAR(50)";
exports.Product = db.define({
    name: 'Product',
    columns: {
        id: { primaryKey: true, notNull: true, dataType: TYPE_UUID },
        name: { dataType: "CHAR(20)" },
        img: { dataType: "TEXT" },
        marketId: { dataType: TYPE_UUID },
        action: { dataType: "BOOLEAN" }
    }
});
//# sourceMappingURL=product.js.map