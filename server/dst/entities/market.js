"use strict";
var db = require('../lib/base');
var TYPE_UUID = "CHAR(36)", TYPE_USER_ID = "VARCHAR(50)";
exports.Market = db.define({
    name: 'Market',
    columns: {
        id: { primaryKey: true, notNull: true, dataType: TYPE_UUID },
        name: { dataType: "CHAR(20)" },
        map: { dataType: "TEXT" },
        imgUrl: { dataType: "TEXT" }
    }
});
//# sourceMappingURL=market.js.map