"use strict";
var Promise = require('bluebird');
var entities = require('../entities/all');
function getModels() {
    return Object.keys(entities).map(function (modelName) { return entities[modelName]; }).filter(function (table) {
        return (typeof table.create == "function") && (typeof table.drop == "function");
    });
}
function up(tx) {
    return Promise.map(getModels(), function (table) { return table.create().ifNotExists().execWithin(tx); });
}
exports.up = up;
function down(tx) {
    return Promise.map(getModels(), function (table) { return table.drop().ifExists().execWithin(tx); });
}
exports.down = down;
//# sourceMappingURL=000-initial.js.map