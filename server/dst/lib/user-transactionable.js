"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Transactionable = require('./transactionable');
var UserEvent = require('./user-event');
/* istanbul ignore next */
var UserTransactionable = (function (_super) {
    __extends(UserTransactionable, _super);
    function UserTransactionable(opt) {
        _super.call(this, opt);
        /* istanbul ignore if */ // Unexpected error case.
        if (!this.tx)
            throw new TypeError("No transaction in creation options");
        this.ownerId = opt.ownerId;
    }
    UserTransactionable.prototype.event = function (data) {
        return new UserEvent(this.tx, this.ownerId, data);
    };
    /* istanbul ignore next */ // Unused for now.
    UserTransactionable.prototype.assertUid = function () { if (!this.ownerId)
        throw new Error("Method requires ownerId"); };
    return UserTransactionable;
}(Transactionable));
module.exports = UserTransactionable;
//# sourceMappingURL=user-transactionable.js.map