"use strict";
/** Determines whether the server is running in a test environment. */
/* istanbul ignore next */
function isTest() { return process.env['NODE_ENV'] == 'test'; }
exports.isTest = isTest;
//# sourceMappingURL=env-utils.js.map