"use strict";
var _ = require('lodash');
var Promise = require('bluebird');
var config = require('../config/index');
/* istanbul ignore next */
function defaultOptions(opt) {
    return _.defaults(opt || {}, { maxChunkSize: config.maxListLengthInWhere, postWhereProcessing: _.identity });
}
/**
 * Splits a query into several and combines the result. This is intended for queries that take lists,
 * in case the list gets too big for one call.
 * @param tx the database transaction to use.
 * @param query the query to split. The query should not include the WHERE clause that has the list.
 * @param chunkedIn details on the chunked part.
 * @param chunkedIn.column the column to filter by.
 * @param chunkedIn.values the values of the column to filter by.
 * @param [opt] additional options.
 * @param [opt.maxChunkSize] the number of values that can be processed before a new query is split off.
 * 						 	 If omitted, will use the server-configured default.
 * @param [opt.postWhereProcessing] a function that can be executed on the query before it is executed (should return another query).
 * @param <T1> the result type.
 * @param <T2> the type of the chunked data.
 * @returns {Promise<T1>} resolves to the collected results of the chunked query.
 */
/* istanbul ignore next */
function isIn(tx, query, chunkedIn, opt) {
    if (chunkedIn.values.length == 0)
        Promise.resolve([]);
    opt = defaultOptions(opt);
    return Promise.map(_.chunk(chunkedIn.values, opt.maxChunkSize), function (values) {
        return opt.postWhereProcessing(query.where(chunkedIn.column.in(values))).allWithin(tx);
    }).then(function (chunks) { return _.flatten(chunks); });
}
exports.isIn = isIn;
/**
 * Splits a query into several and combines the result. This is intended for queries that take lists,
 * in case the list gets too big for one call.
 * @param tx the database transaction to use.
 * @param query the query to split. The query should not include the WHERE clause that has the list.
 * @param chunkedNotIn details on the chunked part.
 * @param chunkedNotIn.column the column to filter by.
 * @param chunkedNotIn.values the values of the column to filter by.
 * @param [maxChunkSize] the number of values that can be processed before a new query is split off.
 * 						 If omitted, will use the server-configured default.
 * @param <T1> the result type.
 * @param <T2> the type of the chunked data.
 * @returns {Promise<T1>} resolves to the collected results of the chunked query.
 */
/* istanbul ignore next */
function isNotIn(tx, query, chunkedNotIn, opt) {
    if (chunkedNotIn.values.length == 0)
        Promise.resolve([]);
    opt = defaultOptions(opt);
    return Promise.map(_.chunk(chunkedNotIn.values, opt.maxChunkSize), function (values) {
        return opt.postWhereProcessing(query.where(chunkedNotIn.column.notIn(values))).allWithin(tx);
    }).then(function (chunks) { return _.flatten(chunks); });
}
exports.isNotIn = isNotIn;
//# sourceMappingURL=chunked-query.js.map