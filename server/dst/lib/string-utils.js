"use strict";
/**
 * Determines whether a string ends with another string.
 * @param {String} str the string.
 * @param {String} suffix the suffix.
 * @returns {boolean} a value indicating whether the string ends with the specified suffix.
 */
function _endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) != -1;
}
/**
* Determines whether a string ends with one of several other strings.
* @param {String} str the string.
* @param {String[]} suffixes the potential suffixes.
* @returns {boolean} a value indicating whether the string ends with at least one of the specified suffixes.
*/
function endsWith(str) {
    var suffixes = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        suffixes[_i - 1] = arguments[_i];
    }
    for (var i = 0; i < suffixes.length; i++)
        if (_endsWith(str, suffixes[i]))
            return true;
    return false;
}
exports.endsWith = endsWith;
/**
 * Determines whether a string starts with another string.
 * @param str the string.
 * @param prefix the prefix.
 * @returns {boolean} a value indicating whether the string starts with the specified prefix.
 */
/* istanbul ignore next */
function startsWith(str, prefix) { return str.indexOf(prefix) == 0; }
exports.startsWith = startsWith;
//# sourceMappingURL=string-utils.js.map