"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Event = require('./event');
/* istanbul ignore next */
var UserEvent = (function (_super) {
    __extends(UserEvent, _super);
    function UserEvent(tx, uid, data) {
        _super.call(this, tx, data);
        this.tx = tx;
        this.uid = uid;
        this.data = data;
    }
    UserEvent.prototype.new = function (data) {
        return new UserEvent(this.tx, this.uid, data);
    };
    return UserEvent;
}(Event));
module.exports = UserEvent;
//# sourceMappingURL=user-event.js.map