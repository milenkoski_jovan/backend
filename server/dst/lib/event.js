/**
 * Created by spion on 11/13/14.
 */
"use strict";
/* istanbul ignore next */
var Event = (function () {
    function Event(tx, data) {
        this.tx = tx;
        this.data = data;
    }
    /* istanbul ignore next */ // Not testing this, seems to be unused.
    Event.prototype.new = function (data) {
        return new Event(this.tx, data);
    };
    return Event;
}());
module.exports = Event;
//# sourceMappingURL=event.js.map