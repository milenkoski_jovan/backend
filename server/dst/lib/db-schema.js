"use strict";
// Works for SQLite in testing and MySQL in all other environments.
var url = require('url');
var _ = require('lodash');
var env = require('./env-utils');
var config = require('../config/index');
function getDatabaseUrl() { return config.database.url; }
/**
 * Gets the name of the database for the current configuration.
 * @return {String} the name of the database for the current configuration.
 */
function getDatabaseName() { return _.last(getDatabaseUrl().split('/')); }
exports.getDatabaseName = getDatabaseName;
/** Gets the database dialect. */
function extractDialect() {
    var dialect = url.parse(getDatabaseUrl()).protocol;
    dialect = dialect.substr(0, dialect.length - 1);
    if (dialect == 'sqlite3')
        dialect = 'sqlite';
    return dialect;
}
exports.extractDialect = extractDialect;
/**
 * Checks if a column exists in a table in the current database.
 * @param tx the transaction to use to make the check query.
 * @param tableName the name of the table.
 * @param columnName the name of the column.
 * @return {Promise<boolean>} resolves to a value indicating whether the specified column exists.
 */
function columnExistsByName(tx, tableName, columnName) {
    if (env.isTest()) {
        return tx.queryAsync('PRAGMA table_info(' + tableName + ');').then(function (res) {
            return _.any(res.rows, function (row) { return row.name == columnName; });
        });
    }
    else {
        return tx.queryAsync("SELECT * " +
            "FROM information_schema.COLUMNS " +
            "WHERE TABLE_SCHEMA = '" + getDatabaseName() + "' AND " +
            "TABLE_NAME = '" + tableName + "' AND " +
            "COLUMN_NAME = '" + columnName + "'").then(function (res) { return res.rowCount > 0; });
    }
}
exports.columnExistsByName = columnExistsByName;
/**
 * Checks if a column exists in the current database.
 * @param tx the transaction to use to make the check query.
 * @param column the column to check.
 * @return {Promise<boolean>} resolves to a value indicating whether the specified column exists.
 */
function columnExists(tx, column) {
    return columnExistsByName(tx, column.table._name, column.name);
}
exports.columnExists = columnExists;
//# sourceMappingURL=db-schema.js.map