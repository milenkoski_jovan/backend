"use strict";
/* istanbul ignore next */ // Not testing this.
function CodedError(code, message /*, err?:Error*/) {
    var self = new Error(message);
    self.code = code;
    //    if (err) {
    //        self.innerError = err;
    //        self.stack += err.stack;
    //    }
    return self;
}
module.exports = CodedError;
//# sourceMappingURL=coded-error.js.map