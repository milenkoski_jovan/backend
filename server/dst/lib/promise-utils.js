"use strict";
var Promise = require('bluebird');
/* istanbul ignore next */
function mapSeries(items, mapFn) {
    var current = Promise.resolve();
    if (!items)
        throw new Error("Items array not provided");
    if (!items.map)
        throw new TypeError("Items are not an array: " + items);
    var results = items.map(function (item, index) {
        return (current = current.then(function (_) { return mapFn(item, index); }));
    });
    return Promise.all(results);
}
exports.mapSeries = mapSeries;
/**
 * Nested version of {@link mapSeries}.
 * @param {Array} items an array of items to map.
 * @param {...Function} fn mapping functions. The first function in the series maps <code>items</code>, while each
 *                         following function takes the result from the preceding function and maps it to another value.
 *                         The final function is the one that is executed. If only one function is provided, this is (or
 *                         at least should be) equivalent to calling <code>mapSeries</code>.
 * @returns {Promise<Array>} resolves to an array of the results of the mapping function applied to each item.
 */
/* istanbul ignore next */
function mapSeriesNested(items, fn) {
    var restOfFunctions = [];
    for (var _i = 2; _i < arguments.length; _i++) {
        restOfFunctions[_i - 2] = arguments[_i];
    }
    if (arguments.length <= 2)
        return mapSeries(items, fn);
    var current = Promise.resolve();
    var results = [];
    items.map(fn).forEach(function (mappableArray) {
        current = current.then(function () {
            return mapSeriesNested.apply(null, [mappableArray].concat(restOfFunctions));
        }).then(function (res) { results = results.concat(res); });
    });
    return current.then(function () { return results; });
}
exports.mapSeriesNested = mapSeriesNested;
/**
 * Like Bluebird's {@link Promise.props()}, only the promises are resolved in sequence instead of in parallel.
 * @param {Object} items the object to resolve.
 * @returns {Promise<Object>} resolves to an object matching the resolved properties of <code>items</code>.
 */
/* istanbul ignore next */
function propsSeries(items) {
    var current = Promise.resolve();
    var results = {};
    for (var key in items)
        if (items.hasOwnProperty(key))
            (function (key) {
                current = current.return(items[key]).then(function (result) { results[key] = result; });
            })(key);
    return current.then(function () { return results; });
}
exports.propsSeries = propsSeries;
/**
 * Like Bluebird's {@link Promise.any()}, only returns a <code>true</code> or <code>false</code> value depending on
 * whether any of the specified were fulfilled.
 * @param items a list of promises to look over.
 * @returns {Promise<boolean>}
 */
/* istanbul ignore next */
function anyTrueFalse(items) {
    return Promise.any(items).then(function () { return true; }).error(function (err) {
        if ((err.name == 'AggregateError') && (err.length == items.length))
            return false;
        throw err;
    });
}
exports.anyTrueFalse = anyTrueFalse;
//# sourceMappingURL=promise-utils.js.map