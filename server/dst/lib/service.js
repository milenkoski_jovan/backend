"use strict";
var bl = require('bl');
var _ = require('lodash');
var http = require('http');
var https = require('https');
var qs = require('querystring');
var Promise = require('bluebird');
var through = require('through2');
var duplexer = require('duplexer2');
var strUtils = require('./string-utils');
/* istanbul ignore next */ // This never happens, but feel free to add it back if it does.
function replaceAll(str, context) {
    var reg = /:([^\/]+)/;
    var match;
    var names = {};
    while ((match = str.match(reg))) {
        var name = match[1], inner = match[0];
        str = str.substr(0, match.index) + context[name] + str.substr(match.index + inner.length);
        names[name] = true;
    }
    return str;
}
/* istanbul ignore next */
var Service = (function () {
    function Service(baseUrl) {
        this.baseUrl = baseUrl;
        var parts = baseUrl.split("/");
        this.protocol = parts[0].substring(0, parts[0].length - 1);
        var parts2 = parts[2].split(":");
        this.hostname = parts2[0];
        if (parts2[1])
            this.port = parseInt(parts2[1]);
        this.defaultPath = parts.slice(3).join("/");
        if (!strUtils.startsWith(this.defaultPath, "/"))
            this.defaultPath = "/" + this.defaultPath;
        console.log("Service wrapper initialized", baseUrl);
    }
    Service.prototype.getDefaultOpt = function (headers) {
        if (headers === void 0) { headers = {}; }
        return { headers: _.defaults(headers, {
                'X-Requested-With': "XMLHttpRequest"
            }) };
    };
    Service.prototype.request = function (opt) {
        console.log('opt request', opt);
        var p = Promise.defer();
        var expectedCode = 200;
        var path = replaceAll(opt.url, opt.query);
        if (strUtils.startsWith(path, "/") && strUtils.endsWith(this.defaultPath, "/"))
            path = this.defaultPath.substring(0, this.defaultPath.length - 1) + path;
        else
            path = this.defaultPath + path;
        var reqOptions = {
            hostname: this.hostname,
            method: opt.method,
            path: path,
            port: this.port,
            headers: opt.headers || {}
        };
        console.log('req options', reqOptions);
        var postdata = undefined;
        if (opt.body != null) {
            postdata = new Buffer(JSON.stringify(opt.body));
            reqOptions.headers['Content-Type'] = 'application/json';
            reqOptions.headers['Content-Length'] = postdata.length;
        }
        if (opt.query != null)
            reqOptions.path += '?' + qs.stringify(opt.query);
        var inp = through(), out = through();
        function handleRequest(res) {
            res.on('error', p.reject.bind(p));
            res.pipe(new bl(function (err, buf) {
                if (err)
                    return p.reject(err);
                var unparsed = res.body = buf.toString();
                if (opt.json)
                    try {
                        if (res.body.indexOf("Cannot " + reqOptions.method.toUpperCase()) == 0)
                            err = new Error("Missing endpoint: " + reqOptions.path);
                        else
                            res.body = JSON.parse(res.body);
                    }
                    catch (e) {
                        err = new Error(res.body);
                    }
                if (res.statusCode != expectedCode) {
                    if (!err) {
                        var msg = res.body.stack || unparsed;
                        err = new Error("HTTP code " + res.code + ' ' + msg);
                    }
                }
                p.callback(err, res);
            }));
            res.pipe(out);
        }
        var request;
        switch (this.protocol.toLowerCase()) {
            case "http":
                request = http.request;
                break;
            case "https":
                request = https.request;
                break;
            default: throw new Error("Unknown protocol: " + this.protocol);
        }
        var req = request(reqOptions, handleRequest);
        req.on('error', p.reject.bind(p));
        if (opt.body != null || opt.method == 'GET' || opt.method == 'DELETE')
            req.end(postdata);
        else
            inp.pipe(req);
        var ret = duplexer(inp, out);
        ret.then = function (succ, err) { return p.promise.then(succ, err); };
        function expect(code) {
            expectedCode = code;
            return ret;
        }
        ret.expect = expect;
        ret.promise = function () { return p.promise; };
        return ret;
    };
    Service.prototype._get = function (url, query, opt) {
        if (query === void 0) { query = {}; }
        if (opt === void 0) { opt = this.getDefaultOpt(); }
        opt.url = url;
        opt.method = 'GET';
        opt.query = query || {};
        console.log("opt", opt);
        return this.request(opt);
    };
    Service.prototype._post = function (url, body, opt) {
        if (opt === void 0) { opt = this.getDefaultOpt(); }
        opt.url = url;
        opt.method = 'POST';
        opt.body = body;
        return this.request(opt);
    };
    Service.prototype._delete = function (url, opt) {
        if (opt === void 0) { opt = this.getDefaultOpt(); }
        opt.url = url;
        opt.method = 'DELETE';
        return this.request(opt);
    };
    Service.prototype.getJSON = function (url, query, opt) {
        if (query === void 0) { query = {}; }
        if (opt === void 0) { opt = this.getDefaultOpt(); }
        console.log('get json url: ', url);
        opt.json = true;
        return this._get(url, query, opt);
    };
    Service.prototype.postJSON = function (url, body, opt) {
        if (opt === void 0) { opt = this.getDefaultOpt(); }
        opt.json = true;
        return this._post(url, body, opt);
    };
    Service.prototype.deleteJSON = function (url, opt) {
        if (opt === void 0) { opt = this.getDefaultOpt(); }
        opt.json = true;
        return this._delete(url, opt);
    };
    Service.prototype.post = function (url, what, headers) {
        if (headers === void 0) { headers = {}; }
        return this.postJSON(url, what, this.getDefaultOpt(headers));
    };
    Service.prototype.get = function (url, qs, headers) {
        if (headers === void 0) { headers = {}; }
        console.log('url: ', url);
        return this.getJSON(url, qs, this.getDefaultOpt(headers));
    };
    Service.prototype.delete = function (url, headers) {
        if (headers === void 0) { headers = {}; }
        return this.deleteJSON(url, this.getDefaultOpt(headers));
    };
    Service.prototype.postRaw = function (url, what) { return this._post(url, what, this.getDefaultOpt()); };
    Service.prototype.getRaw = function (url, qs) { return this._get(url, qs, this.getDefaultOpt()); };
    Service.extractBody = function (response) { return response.body; };
    return Service;
}());
module.exports = Service;
//# sourceMappingURL=service.js.map