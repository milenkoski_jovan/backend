"use strict";
var codedError = require('../lib/coded-error');
var _ = require('lodash');
/* istanbul ignore next */
(function (Type) {
    Type[Type["String"] = "string"] = "String";
    Type[Type["Number"] = "number"] = "Number";
    Type[Type["Object"] = "object"] = "Object";
})(exports.Type || (exports.Type = {}));
var Type = exports.Type;
/* istanbul ignore next */
(function (Qualifier) {
    Qualifier[Qualifier["MoreThan"] = "moreThan"] = "MoreThan";
    Qualifier[Qualifier["LessThan"] = "lessThan"] = "LessThan";
    Qualifier[Qualifier["Equals"] = "equals"] = "Equals";
    Qualifier[Qualifier["NotEquals"] = "notEquals"] = "NotEquals";
    Qualifier[Qualifier["NotEmpty"] = "notEmpty"] = "NotEmpty";
    Qualifier[Qualifier["NoQualifier"] = "noQualifier"] = "NoQualifier";
})(exports.Qualifier || (exports.Qualifier = {}));
var Qualifier = exports.Qualifier;
/* istanbul ignore next */
function check(value, type, criteria, errorMsg) {
    if (!value)
        throw codedError(400, errorMsg || "Invalid value");
    switch (type) {
        case Type.String: {
            // TODO: Implement case content
            handleStringValue(value, type, criteria, errorMsg);
            break;
        }
        case Type.Number: {
            handleNumberValue(value, type, criteria, errorMsg);
            break;
        }
        case Type.Object: {
            if (criteria.qualifier == Qualifier.NotEmpty) {
                if (_.isEmpty(value))
                    throw codedError(400, errorMsg || 'Invalid object - object is empty');
            }
            break;
        }
        default: {
            // TODO: Implemente default case
            throw codedError(400, 'Type not provided');
        }
    }
}
exports.check = check;
/* istanbul ignore next */
function handleStringValue(value, type, criteria, errorMsg) {
    if (typeof (value) !== "string")
        throw codedError(400, errorMsg || "Invalid value - not a string");
    var qualifier = criteria.qualifier;
    if (criteria) {
        if (criteria.value) {
            if (qualifier == Qualifier.Equals) {
                if (value !== criteria.value) {
                    throw codedError(400, errorMsg || "Invalid value - strings are not equal");
                }
            }
            else if (qualifier == Qualifier.NotEquals) {
                if (value == criteria.value) {
                    throw codedError(400, errorMsg || 'Invalud value - strings are equal');
                }
            }
        }
        if (criteria.length) {
            if (qualifier == Qualifier.Equals) {
                if (value.length !== criteria.length) {
                    throw codedError(400, errorMsg || "Invalid value - string has invalid length");
                }
            }
            else if (qualifier == Qualifier.NotEquals) {
                if (value.length !== criteria.length) {
                    throw codedError(400, errorMsg || "Invalud value - String has invalid length");
                }
            }
        }
        if (criteria.length) {
            throw codedError(400, 'Cannot check length of number');
        }
    }
}
/* istanbul ignore next */
function handleNumberValue(value, type, criteria, errorMsg) {
    if (typeof (value) !== "number")
        throw codedError(400, errorMsg || "Invalid value - not a number");
    if (criteria) {
        var qualifier = Qualifier.Equals;
        if (criteria.qualifier)
            qualifier = criteria.qualifier;
        if (criteria.value) {
            if (qualifier == Qualifier.Equals) {
            }
            else if (qualifier == Qualifier.MoreThan) {
            }
            else if (qualifier == Qualifier.LessThan) {
            }
        }
        if (criteria.length) {
            throw codedError(400, 'Cannot check length of number');
        }
    }
}
//# sourceMappingURL=checker.js.map