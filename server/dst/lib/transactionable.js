"use strict";
var chunkedQuery = require('./chunked-query');
/* istanbul ignore next */
var Transactionable = (function () {
    function Transactionable(opt) {
        this.tx = opt.tx;
    }
    Transactionable.prototype.isIn = function (query, chunkedIn, opt) {
        return chunkedQuery.isIn(this.tx, query, chunkedIn, opt);
    };
    Transactionable.prototype.isNotIn = function (query, chunkedNotIn, opt) {
        return chunkedQuery.isNotIn(this.tx, query, chunkedNotIn, opt);
    };
    return Transactionable;
}());
module.exports = Transactionable;
//# sourceMappingURL=transactionable.js.map