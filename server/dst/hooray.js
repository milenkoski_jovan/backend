"use strict";
require('source-map-support').install();
var express = require('express');
var path = require('path');
var fs = require('fs');
var app = express();
console.log("Configuring express...");
app.set('port', process.env.PORT || 9300);
app.enable('trust proxy');
/* istanbul ignore next */
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.type('text/plain');
    next();
});
app.use(require('prerender-node').set('prerenderToken', 'icVzBhRcOJF59fbKUhf9'));
/* istanbul ignore if */ // Can't test this.
if (app.get("env") == 'development') {
    app.use(express.errorHandler());
    app.use(express.logger('dev'));
}
;
var exts = /\.js$/, routedir = path.join(__dirname, 'routes');
console.log("Setting up routes...");
fs.readdirSync(routedir).forEach(function (f) {
    if (f[0] != '.' && exts.test(f)) {
        var routePath = path.join(routedir, f);
        var routeModule = require(routePath);
        var usepath = '/' + f.replace(exts, '');
        /* istanbul ignore if */ // this breaks the build process, no need to cover it
        if (!routeModule.route) {
            console.error("Route module " + f + " does not export itself properly");
            throw new Error("Unable to set up route " + f + ". Check the route module for unexported route variables");
        }
        app.use(usepath, routeModule.route);
    }
});
/* istanbul ignore next */
app.use('/', function (req, res) {
    //console.log(req, res);
    if (req.xhr) {
        console.log("xhr at", req.url, "not found");
        return res.status(404).send(JSON.stringify({ message: 'API endpoint not found' }));
    }
});
var wiring = require('./wiring/index');
wiring.initialize();
/* istanbul ignore if */
if (process.env.NODE_ENV !== 'test') {
    app.listen(app.get('port'), function () {
        console.log("Express server listening on port " + app.get('port'));
    });
}
module.exports = app;
//# sourceMappingURL=hooray.js.map