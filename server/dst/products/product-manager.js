"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var _ = require('lodash');
var UserTransactionable = require('../lib/user-transactionable');
var all_1 = require('../entities/all');
var ProductManager = (function (_super) {
    __extends(ProductManager, _super);
    function ProductManager() {
        _super.apply(this, arguments);
    }
    ProductManager.prototype.getAllProducts = function () {
        return all_1.Product.from(all_1.Product.join(all_1.Market).on(all_1.Product.marketId.equals(all_1.Market.id)))
            .select(all_1.Product.star(), all_1.Market.name.as("marketName"), all_1.Market.imgUrl.as("marketImgUrl"))
            .allWithin(this.tx).then(function (products) {
            return _.uniq(products, 'name');
        });
    };
    ProductManager.prototype.getAllMarkets = function () {
        return all_1.Market.select(all_1.Market.star()).allWithin(this.tx);
    };
    ProductManager.prototype.getMarketById = function (marketId) {
        return all_1.Market.select(all_1.Market.star())
            .where(all_1.Market.id.equals(marketId)).getWithin(this.tx);
    };
    ProductManager.prototype.getProductsByMarket = function (marketId) {
        return all_1.Product.select(all_1.Product.star())
            .where(all_1.Product.marketId.equals(marketId)).allWithin(this.tx);
    };
    ProductManager.prototype.getProductsByName = function (productName) {
        return all_1.Product.from(all_1.Product.join(all_1.Market).on(all_1.Product.marketId.equals(all_1.Market.id)))
            .select(all_1.Product.star(), all_1.Market.name.as("marketName"), all_1.Market.imgUrl.as("marketImgUrl"))
            .where(all_1.Product.name.equals(productName))
            .allWithin(this.tx);
    };
    return ProductManager;
}(UserTransactionable));
exports.ProductManager = ProductManager;
function create(opt) {
    return new ProductManager(opt);
}
exports.create = create;
//# sourceMappingURL=product-manager.js.map