"use strict";
var Logic = require('../services/logic');
/* istanbul ignore next */
var EventManager = (function () {
    function EventManager() {
        this.logicService = Logic.create();
    }
    /**
     * Asynchronously posts an event.
     * @param eventName the name of the event.
     * @param data additional event data.
     */
    EventManager.prototype.postEvent = function (eventName, data) { return this.logicService.postEvent(eventName, data, true); };
    return EventManager;
}());
module.exports = EventManager;
//# sourceMappingURL=event-manager.js.map