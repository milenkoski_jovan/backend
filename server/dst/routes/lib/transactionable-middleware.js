"use strict";
var db = require('../../lib/base');
/* istanbul ignore next */
function autoAnswer(res) {
    return function (d) {
        if (d != null)
            res.answer(d);
    };
}
exports.autoAnswer = autoAnswer;
/* istanbul ignore next */
function wrapTx(mw) {
    return function (req, res) {
        var aa = autoAnswer(res);
        db.transaction(function (tx) {
            // tx.logQueries(true);
            var treq = req;
            treq.userTx = { tx: tx, ownerId: null };
            return mw(treq, res);
        }).done(aa, aa);
    };
}
exports.wrapTx = wrapTx;
//# sourceMappingURL=transactionable-middleware.js.map