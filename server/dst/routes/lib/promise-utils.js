"use strict";
var Promise = require('bluebird');
/* istanbul ignore next */
var AsyncTaskChain = (function () {
    function AsyncTaskChain() {
        this.taskChain = Promise.resolve();
    }
    AsyncTaskChain.prototype.pushTask = function (asyncTaskFn) {
        this.taskChain = this.taskChain.then(function () { return (asyncTaskFn); });
    };
    AsyncTaskChain.prototype.promise = function () {
        return this.taskChain;
    };
    return AsyncTaskChain;
}());
exports.AsyncTaskChain = AsyncTaskChain;
//# sourceMappingURL=promise-utils.js.map