"use strict";
var router = require('simple-router');
var TM = require('./lib/transactionable-middleware');
var TagManager = require('../products/product-manager');
var app = router();
app.get("/list/all", TM.wrapTx(function (req) {
    var ProductMng = TagManager.create(req.userTx);
    return ProductMng.getAllMarkets();
}));
app.get("/product/list/all", TM.wrapTx(function (req) {
    var ProductMng = TagManager.create(req.userTx);
    return ProductMng.getAllProducts();
}));
app.get("/:marketId/get", TM.wrapTx(function (req) {
    var ProductMng = TagManager.create(req.userTx);
    return ProductMng.getMarketById(req.params.marketId);
}));
app.get("/:marketId/products/get", TM.wrapTx(function (req) {
    var ProductMng = TagManager.create(req.userTx);
    return ProductMng.getProductsByMarket(req.params.marketId);
}));
app.get("/product/:productName/get", TM.wrapTx(function (req) {
    var ProductMng = TagManager.create(req.userTx);
    return ProductMng.getProductsByName(req.params.productName);
}));
exports.route = app.route;
//# sourceMappingURL=market.js.map