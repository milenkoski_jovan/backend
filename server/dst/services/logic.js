"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Promise = require('bluebird');
var env = require('../lib/env-utils');
var Service = require("../lib/service");
var config = require("../config/index");
/* istanbul ignore next */
var Logic = (function (_super) {
    __extends(Logic, _super);
    function Logic() {
        _super.call(this, config.services.logic.url);
    }
    Logic.prototype.postEvent = function (eventName, eventData, sync) {
        if (sync === void 0) { sync = false; }
        if (env.isTest())
            return Promise.resolve({ done: true });
        return this.post('/event/', { eventName: eventName, eventData: eventData, sync: sync })
            .then(Service.extractBody);
    };
    return Logic;
}(Service));
/* istanbul ignore next */
function create() { return new Logic(); }
exports.create = create;
//# sourceMappingURL=logic.js.map