"use strict";
if (process.env.NODE_ENV != 'test') {
    console.error("Not running in test environment, running in " + process.env.NODE_ENV);
    console.error("Please use the test runner script: scripts/run-test.sh to run the test");
    process.exit();
}
// These lines are pretty much useless now.
process.env.NODE_ENV = 'test';
if (!process.env.NO_BLUEBIRD_DEBUG)
    process.env.BLUEBIRD_DEBUG = 1;
if (!process.env.NO_SOURCEMAPS)
    require('source-map-support').install();
var Promise = require('bluebird');
var t = require('blue-tape');
var db = require('../../lib/base');
var entities = require("../../entities/all");
var models = Object.keys(entities).map(function (modelName) { return entities[modelName]; });
function deleteAll() {
    return Promise.all(models.map(function (model) {
        var p = Promise.resolve();
        if (model.create && model.create().exec) {
            try {
                p = model.create().ifNotExists().all();
            }
            catch (e) {
                console.error("Unable to create database model", model, e);
            }
        }
        p = p.then(function () {
            if (model.delete && model.delete().exec)
                return model.delete().exec();
            else
                return Promise.resolve();
        });
        return p;
    }));
}
function before() {
    db.open();
    return deleteAll();
}
function after(t) {
    db.close();
    t.end();
}
function test(description, test) {
    t.test(' = BEGIN: ' + description, before);
    t.test.apply(t, arguments);
    t.test(' = END: ' + description, after);
}
exports.test = test;
function errorTest(t, action, expectedCode, message) {
    return Promise.try(function () { return action.then(function () { t.notOk(true, message); }); }).catch(function (err) {
        t.equals(err.code, expectedCode, message);
    });
}
exports.errorTest = errorTest;
//# sourceMappingURL=test.js.map