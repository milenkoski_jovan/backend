var fs = require('fs');
var strUtils = require('./dst/lib/string-utils');
var path = require('path');

fs.readdirSync(path.join(__dirname, 'dst', 'test')).filter(function(path) {
    return strUtils.endsWith(path, '.js');
}).forEach(function(filename) { require('./dst/test/' + filename); });