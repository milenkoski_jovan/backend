FROM node:0.12.0-slim
MAINTAINER Elena Trajkovska <elena.trajkovska@alite-international.com>

ADD . /app
RUN cd /app; npm install
EXPOSE 9300
CMD ["node", "/app/server/dst/hooray.js"]
